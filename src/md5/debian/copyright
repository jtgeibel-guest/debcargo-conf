Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: md5
Upstream-Contact:
 Ivan Ukhov <ivan.ukhov@gmail.com>
 Kamal Ahmad <shibe@openmailbox.org>
 Konstantin Stepanov <milezv@gmail.com>
 Lukas Kalbertodt <lukas.kalbertodt@gmail.com>
 Nathan Musoke <nathan.musoke@gmail.com>
 Tony Arcieri <bascule@gmail.com>
 Wim de With <register@dewith.io>
 Yosef Dinerstein <yosefdi@gmail.com>
Source: https://github.com/stainless-steel/md5

Files: *
Copyright:
 2015-2018 Ivan Ukhov <ivan.ukhov@gmail.com>
 2018 Kamal Ahmad <shibe@openmailbox.org>
 2015 Konstantin Stepanov <milezv@gmail.com>
 2016 Lukas Kalbertodt <lukas.kalbertodt@gmail.com>
 2017 Nathan Musoke <nathan.musoke@gmail.com>
 2017 Tony Arcieri <bascule@gmail.com>
 2018 Wim de With <register@dewith.io>
 2018 Yosef Dinerstein <yosefdi@gmail.com>
 2015-2018 The md5 Developers
License: Apache-2.0 or MIT

Files: debian/*
Copyright:
 2018-2019 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2018-2019 kpcyrd <git@rxv.cc>
License: Apache-2.0 or MIT

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
